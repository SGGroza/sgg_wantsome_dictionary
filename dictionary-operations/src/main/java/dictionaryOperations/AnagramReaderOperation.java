package dictionaryOperations;

import jdk.nashorn.internal.ir.IfNode;
import sun.applet.Main;
import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AnagramReaderOperation implements DictionaryOperations {
    private Set<String> wordsSet;
    private BufferedReader in;

    public AnagramReaderOperation(Set<String> wordsSet, BufferedReader in) {
        this.wordsSet = wordsSet;
        this.in = in;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Input a word to search for anagrams in Dex - Or:\n" +
                "type 1 for suggestions\n" +
                "type 0 to exit in main Menu");

        String userInput = in.readLine();
        String sortUserInput = Utils.sortLettersInWords(userInput);
        System.out.println("User input: " + userInput + "\n");

        switch (userInput) {
            case "1":
                //        Get anagrams in Dex
                Map<String, List<String>> dexAnagrams = Utils.findAnagramWords(wordsSet);
                for (List anagrams : dexAnagrams.values())
                    if (anagrams.size() > 1) {
                        System.out.println("Anagrams in Dex: " + anagrams);
                    }
                System.out.println("\n");
                run();

            case "0":
                break;

            default:
                //        Get anagrams for userInput word
                Map<String, List<String>> anagramsOfInputWord = Utils.findAnagramWords(wordsSet);

                int count = 0;
                for (Map.Entry myInput : anagramsOfInputWord.entrySet()) {
                    if (myInput.getKey().equals(sortUserInput)) {
                        System.out.println("Anagrams in Dex for \"" + userInput + "\": " + myInput.getValue());
                        count++;
                    }
                }
                if (count == 0) {
                    System.out.println("There is no anagram for the given word.\n" + "PLS try again!");
                    run();
                }
        }
    }
}
