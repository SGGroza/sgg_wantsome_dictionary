package dictionaryOperations;

import java.io.IOException;

public interface DictionaryOperations {
    void run() throws IOException;
}
