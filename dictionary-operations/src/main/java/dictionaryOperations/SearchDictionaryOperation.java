package dictionaryOperations;

import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Set;

public class SearchDictionaryOperation implements DictionaryOperations {
    private Set<String> wordsSet;
    private BufferedReader in;

    public SearchDictionaryOperation(Set<String> wordsSet, BufferedReader in){
        this.wordsSet = wordsSet;
        this.in = in;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Searching ....");

        String userInput = in.readLine();
        System.out.println(" From user: " + userInput);

        Set<String> result = Utils.findDictionaryWords(wordsSet,userInput);

        for (String line : result){
            System.out.println(line);
        }




    }
}
