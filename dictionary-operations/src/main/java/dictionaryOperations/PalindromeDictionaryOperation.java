package dictionaryOperations;

import utils.Utils;

import java.io.IOException;
import java.util.Set;

public class PalindromeDictionaryOperation implements DictionaryOperations {

    private Set<String> wordsSet;

    public PalindromeDictionaryOperation(Set<String> wordsSet) {
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Palindromes .....");
        Set<String> palindromes = Utils.findPalindromesWords(wordsSet);
        for (String line : palindromes) {
            System.out.println(line);

        }
        System.out.println("Done!");
    }
}
