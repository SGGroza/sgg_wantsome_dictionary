import dictionaryOperations.AnagramReaderOperation;
import dictionaryOperations.DictionaryOperations;
import dictionaryOperations.PalindromeDictionaryOperation;
import dictionaryOperations.SearchDictionaryOperation;
import fileReader.ResourceInputFileReader;
import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {

// Read keyboard
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

// Read dictionary
        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("dex.txt");

// No duplicates
        Set<String> wordsNoDuplicates = Utils.removeDuplicate(words);

// Menu
        while (true) {
            System.out.println("Menu: \n" +
                    "1. Search \n" +
                    "2. Find all palindromes \n" +
                    "3. Anagrams \n" +
                    "0. Exit");

// Print input
            String userInput = in.readLine();

// Exit condition
            if (userInput.equals("0")) {
                System.out.println("La revedere !!!");
                break;
            }
// Operator
            DictionaryOperations operation = null;

// Menu options selector
            switch (userInput) {
                case "1":
                    operation = new SearchDictionaryOperation(wordsNoDuplicates, in);
                    break;
                case "2":
                    operation = new PalindromeDictionaryOperation(wordsNoDuplicates);
                    break;
                case "3":
                    operation = new AnagramReaderOperation(wordsNoDuplicates,in);
                    break;
                default:
                    System.out.println("Invalid option");
            }

// Run operation
            if (operation != null) {
                operation.run();
            }
        }
    }
}
