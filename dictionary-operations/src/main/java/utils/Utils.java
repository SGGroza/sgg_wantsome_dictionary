package utils;

import java.util.*;

public class Utils {

    // Deduplication method
    public static Set<String> removeDuplicate(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();

        for (String line : allLines) {
            wordsSet.add(line);
        }
        return wordsSet;
    }

    // Search method
    public static Set<String> findDictionaryWords(Set<String> wordsSet, String word) {
        Set<String> result = new HashSet<>();

        for (String line : wordsSet) {
            if (line.contains(word)) {
                result.add(line);
            }
        }
        return result;
    }

    // Palindromes
    public static Set<String> findPalindromesWords(Set<String> wordsSet) {
        Set<String> result = new HashSet<>();

        for (String line : wordsSet) {
            StringBuilder reverseWord = new StringBuilder(line).reverse();
            if (line.equals(reverseWord.toString())) {
                result.add(line);
            }
        }
        return result;
    }

    //Sort letters in a word method
    public static String sortLettersInWords(String word) {
        String allLowerCase = word.toLowerCase();
        allLowerCase = allLowerCase.replaceAll("\\s+", "");
        char[] sortInput = allLowerCase.toCharArray();
        Arrays.sort(sortInput);
        return String.valueOf(sortInput);

    }


    // Anagrams
    public static Map<String, List<String>> findAnagramWords(Set<String> wordsSet) {
        Map<String, List<String>> anagramsSelector = new HashMap<>();

        for (String line : wordsSet) {
            String key = sortLettersInWords(line);
            if (!anagramsSelector.containsKey(key)) {
                anagramsSelector.put(key, new ArrayList<String>());
            }
            anagramsSelector.get(key).add(line);
        }
        return anagramsSelector;
    }
}