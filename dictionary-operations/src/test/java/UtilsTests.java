import dictionaryOperations.AnagramReaderOperation;
import fileReader.ResourceInputFileReader;
import org.junit.Test;
import utils.Utils;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class UtilsTests {

    @Test
    public void testPalindromeFinder() {
        Set<String> inputSet = new HashSet<>();
        inputSet.add("unu");
        Set<String> palindromes = Utils.findPalindromesWords(inputSet);
        assertTrue(palindromes.contains("unu"));
        assertEquals(1, palindromes.size());
    }

    @Test
    public void sortWordsTest() {
        String myString = "Well this is a test to!";
        String sortedMyString = Utils.sortLettersInWords(myString);
        System.out.println("------- Testing sort algorithm ------- ");
        System.out.println(sortedMyString);
        System.out.println("----------------------\n");
    }

    @Test
    public void anagramsTest() {
        System.out.println("------- Testing anagrams algorithm ------- ");
        Set<String> inputSet = new HashSet<>();
        inputSet.add("Race");
        inputSet.add("part");
        inputSet.add("Heart");
        inputSet.add("knee");
        inputSet.add("ca Re");
        inputSet.add("trap");
        inputSet.add("earth");
        inputSet.add("Keen");

        Map<String, List<String>> myAnagrams = Utils.findAnagramWords(inputSet);
        for (Entry anagrams : myAnagrams.entrySet()) {
            System.out.println("For input letters: " + anagrams.getKey() + " - " + anagrams.getValue());
        }

        assertTrue(myAnagrams.size() != 0);
        assertEquals(myAnagrams.size(),4);

        System.out.println("----------------------\n");
    }

    @Test
    public void anagramsDexFinderTest() throws IOException {
        // Read dictionary
        System.out.println("------- Finding all anagrams in Dex  ------- ");
        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("dex.txt");

        Set<String> wordsNoDuplicates = Utils.removeDuplicate(words);

        Map<String, List<String>> dexAnagrams = Utils.findAnagramWords(wordsNoDuplicates);
        for (List anagrams : dexAnagrams.values())
            if (anagrams.size() > 1) {
                System.out.println("Anagrams in Dex: " + anagrams);
            }
        assertTrue(dexAnagrams.size() != 0);
        System.out.println("----------------------\n");
    }
}
